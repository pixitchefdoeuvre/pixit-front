module.exports = {
  purge: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      height: {
        wide: "90vh",
        half: "50vh",
        fit: "fit-content"
      },
      width: {
        wide: "90vw",
        half: "50vw",
        fit: "fit-content"
      },
      minHeight: {
        "0": "0",
        "4": "1rem",
        "8": "2rem",
        "12": "3rem",
        "1/4": "25%",
        "1/2": "50%",
        "3/4": "75%",
        wide: "90vh",
        half: "50vh",
        full: "100%"
      },
      minWidth: {
        "0": "0",
        "4": "1rem",
        "8": "2rem",
        "12": "3rem",
        "1/4": "25%",
        "1/2": "50%",
        "3/4": "75%",
        wide: "90vw",
        half: "50vw",
        full: "100%"
      },
      maxHeight: {
        "0": "0",
        "1/4": "25%",
        "1/2": "50%",
        "3/4": "75%",
        wide: "90vh",
        half: "50vh",
        full: "100%"
      },
      maxWidth: {
        "0": "0",
        "1/4": "25%",
        "1/2": "50%",
        "3/4": "75%",
        wide: "90vw",
        half: "50vw",
        full: "100%"
      },
      colors: {
        primary: "#00bbf9",
        light: "#faf9f9",
        dark: "#1d3557",
        success: "#3ac661",
        danger: "#d62828",
        link: "#0028f9"
      },
      backgroundColor: ["active"],
      color: ["active"]
    }
  },
  variants: {
    extend: {}
  },
  plugins: []
};
