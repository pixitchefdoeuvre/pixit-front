import { Shape } from "@/assets/Shape";

export interface HistoryItem {
  type: "shape" | "clear"; // TODO | "fillColor" | "strokeColor" | "strokeWidth" | "stopDrawing" | "move"
  value: unknown; // TODO | typeof initialFillColor | number | Point
}

export interface ShapeHistoryItem extends HistoryItem {
  type: "shape";
  value: Shape;
}

export interface ClearHistoryItem extends HistoryItem {
  type: "clear";
  value: number;
}
